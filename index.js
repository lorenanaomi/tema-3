
const FIRST_NAME = "Lorena";
const LAST_NAME = "Spinu";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }

    getDetails(){
        return this.name+" "+this.surname+" "+this.salary;
    }
}

class SoftwareEngineer extends Employee {
      

    constructor(name, surname, salary, experience="JUNIOR"){ //valoare default pt experienta
        super(name,surname,salary);
        this.experience=experience;
    }

    applyBonus(){
        if(this.experience=="JUNIOR"){
            return this.salary + this.salary*10/100;
        }
        else if(this.experience=="MIDDLE"){
            return this.salary + this.salary*15/100;
        }
        else if(this.experience=="SENIOR"){
            return this.salary + this.salary*20/100;
        }
        else{
            return this.salary + this.salary*10/100;
        }
    } 

   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

